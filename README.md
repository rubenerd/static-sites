# static-sites

This serves as a backup for various static sites I host for myself and my family. With one git clone in `/var` from a trusty Ansible script, I can restore several sites to my cloud instance, and track their changes.

My primary site and blog is over on [Rubenerd] thesedays, which is hosted in its [own repo].

## Licence

© Ruben Schade. Text, including HTML and markdown, is dual-licenced under [Creative Commons Attribution 3.0] and the [3-Clause BSD Licence]; the one with the no endorsement clause, which I think is perfectly reasonable.

[Rubenerd]: https://rubenerd.com/
[own repo]: https://codeberg.org/rubenerd/rubenerd.com
[Creative Commons Attribution 3.0]: https://creativecommons.org/licenses/by/3.0/
[3-Clause BSD Licence]: http://opensource.org/licenses/BSD-3-Clause

